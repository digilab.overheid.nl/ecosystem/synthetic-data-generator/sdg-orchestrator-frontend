package process

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

type Process struct {
	ctx    context.Context
	cancel context.CancelFunc
	logger *slog.Logger
}

func New(logger *slog.Logger) *Process {
	ctx, cancel := context.WithCancel(context.Background())

	proc := &Process{
		ctx:    ctx,
		cancel: cancel,
		logger: logger.WithGroup("process"),
	}

	proc.start()

	return proc
}

func (p *Process) start() {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)

	go func() {
		<-sigChan
		p.logger.Info("process cancelled")
		p.cancel()
	}()
}

func (p *Process) Wait() {
	p.logger.Info("process waiting")
	<-p.ctx.Done()
	p.logger.Info("process wait finished")
}

func (p *Process) Shuwdown() {
	p.logger.Info("process shutdown")
	p.cancel()
}
