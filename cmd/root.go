package cmd

import (
	"log/slog"
	"os"

	"github.com/alecthomas/kong"
)

type Context struct {
	Logger slog.Logger
}

var cli struct {
	Debug bool `env:"APP_DEBUG" default:"false" optional:"" name:"debug" help:"Enable debug mode."`

	Serve ServeCmd `cmd:"serve" help:"Startup server."`
}

func Run() error {
	// Parse the command line.
	ctx := kong.Parse(&cli)

	// Setup the logger.
	logLevel := slog.LevelInfo
	if cli.Debug {
		logLevel = slog.LevelDebug
	}

	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: logLevel,
	}))

	// Call the Run() method of the selected parsed command.
	err := ctx.Run(&Context{Logger: *logger})

	return err
}
