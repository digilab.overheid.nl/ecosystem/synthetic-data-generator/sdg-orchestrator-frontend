package cmd

import (
	"errors"
	"net/http"

	_ "github.com/alecthomas/kong-yaml"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-frontend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-frontend/application/service"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-frontend/process"
)

type ServeCmd struct {
	BackendListenAddress string `env:"APP_LISTEN_ADDRESS" default:":8080" name:"listen-address" help:"Address to listen  on."`
	OrchestratorAddress  string `env:"APP_ORCHESTRATOR_ADDRESS" name:"orchestrator-address" help:"Address to orchestrator is available on"`
}

func (opt *ServeCmd) Run(ctx *Context) error {
	logger := ctx.Logger.With("application", "http_server")
	logger.Info("Starting orchestrator frontend", "config", opt)

	proc := process.New(logger)

	servicer := service.New(logger, opt.OrchestratorAddress)

	app := application.New(logger, opt.BackendListenAddress, servicer)

	app.Router()

	logger.Info("Starting server", "address", opt.BackendListenAddress)

	go func() {
		if err := app.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error("listen and serve failed", "err", err)
		}
		logger.Info("server closed")
	}()

	proc.Wait()

	logger.Info("orchestrator frontend stopped")

	return nil
}
