/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: [
    './application/templates/**/*.html',
    './static/js/**/*.js',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
