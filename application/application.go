package application

import (
	"embed"
	"fmt"
	"html/template"
	"log/slog"
	"net/http"
	"strings"
	"time"

	"github.com/ajg/form"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-frontend/application/service"
)

//go:embed templates/**
var tplFolder embed.FS

type Application struct {
	*http.Server
	client   *http.Client
	logger   *slog.Logger
	servicer service.Service
	local    *time.Location
}

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

func (a *Application) FormatTime(t time.Time) string {
	if t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(a.local).Format("2 Jan 2006 15:04 uur"))
}

func New(logger *slog.Logger, listenAddress string, servicer service.Service) Application {
	return Application{
		Server: &http.Server{
			Addr: listenAddress,
		},
		client:   http.DefaultClient,
		logger:   logger,
		servicer: servicer,
		local:    time.UTC,
	}
}

func (a *Application) index(w http.ResponseWriter, r *http.Request) {
	t := template.New("base.html").
		Funcs(template.FuncMap{
			"formatTime": a.FormatTime,
		})

	patterns := []string{
		"templates/base.html",
		"templates/index.html",
	}

	var err error
	if t, err = t.ParseFS(tplFolder, patterns...); err != nil {
		w.Write([]byte(fmt.Errorf("parse fs failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.ExecuteTemplate(w, "base.html", nil); err != nil {
		w.Write([]byte(fmt.Errorf("executing template failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *Application) status(w http.ResponseWriter, r *http.Request) {
	t := template.New("status.html").
		Funcs(template.FuncMap{
			"formatTime": a.FormatTime,
		})

	patterns := []string{
		"templates/partials/status.html",
	}

	var err error
	if t, err = t.ParseFS(tplFolder, patterns...); err != nil {
		w.Write([]byte(fmt.Errorf("parse fs failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	s, err := a.servicer.GetStatus(r.Context())
	if err != nil {
		a.logger.Error("get status:", "err", err)
		w.Write([]byte(fmt.Errorf("get status: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var data struct {
		Status service.Status
	}
	data.Status = *s

	if err := t.ExecuteTemplate(w, "status.html", data); err != nil {
		w.Write([]byte(fmt.Errorf("executing template failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

type Start struct {
	StartTime      string `form:"start-time"`
	EndTime        string `form:"end-time"`
	NumOfInstances int    `form:"num-instances"`
}

func (a *Application) start(w http.ResponseWriter, r *http.Request) {
	start := new(Start)
	if err := form.NewDecoder(r.Body).Decode(start); err != nil {
		w.Write([]byte(fmt.Errorf("executing start failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	st, err := time.Parse("02-01-2006 15:04", start.StartTime)
	if err != nil {
		w.Write([]byte(fmt.Errorf("time parse: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	et, err := time.Parse("02-01-2006 15:04", start.EndTime)
	if err != nil {
		w.Write([]byte(fmt.Errorf("time parse: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	s := service.Start{
		StartTime:      st,
		EndTime:        et,
		NumOfInstances: start.NumOfInstances,
	}

	a.logger.Info("start params", "start", s)

	if err := a.servicer.Start(r.Context(), s); err != nil {
		w.Write([]byte(fmt.Errorf("executing start failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *Application) reset(w http.ResponseWriter, r *http.Request) {
	if err := a.servicer.Reset(r.Context()); err != nil {
		w.Write([]byte(fmt.Errorf("executing start failed: %w", err).Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *Application) Router() error {
	// Template engine

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", a.index)
	r.Get("/status", a.status)
	r.Post("/start", a.start)
	r.Delete("/reset", a.reset)

	r.Handle("/public/*", http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))

	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		a.logger.Warn("route not found", "uri", r.RequestURI)
		w.WriteHeader(http.StatusNotFound)
	})

	a.Handler = r

	return nil
}
