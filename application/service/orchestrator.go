package service

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func (service *Service) GetStatus(ctx context.Context) (*Status, error) {
	status := new(Status)

	if err := service.do(ctx, http.MethodGet, "/status", nil, status); err != nil {
		return nil, fmt.Errorf("get status failed: %w", err)
	}

	return status, nil
}

func (a *Service) Start(ctx context.Context, start Start) error {
	a.logger.Info("Starting the orchestrator", "body", start)

	var seedBody struct {
		NumOfInstances int `json:"num_instances"`
	}
	seedBody.NumOfInstances = start.NumOfInstances

	if err := a.do(ctx, http.MethodPost, "/seed", seedBody, nil); err != nil {
		return fmt.Errorf("seed failed: %w", err)
	}

	var startBody struct {
		StartTime time.Time `json:"startTime"`
		EndTime   time.Time `json:"endTime"`
	}
	startBody.StartTime = start.StartTime
	startBody.EndTime = start.EndTime

	if err := a.do(ctx, http.MethodPost, "/start", startBody, nil); err != nil {
		return fmt.Errorf("start failed: %w", err)
	}

	return nil
}

func (a *Service) Reset(ctx context.Context) error {
	a.logger.Info("Resetting the orchestrator")
	if err := a.do(ctx, http.MethodDelete, "/reset", nil, nil); err != nil {
		return fmt.Errorf("reset failed: %w", err)
	}

	return nil
}
