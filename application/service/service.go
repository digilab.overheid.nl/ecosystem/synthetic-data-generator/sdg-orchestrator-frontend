package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go/jetstream"
)

type Service struct {
	logger       *slog.Logger
	client       *http.Client
	orchestrator string
}

func New(logger *slog.Logger, orchestrator string) Service {
	return Service{
		logger:       logger.WithGroup("service"),
		client:       http.DefaultClient,
		orchestrator: orchestrator,
	}
}

type State string

type Status struct {
	StartTime            time.Time                             `json:"startTime"`
	EndTime              time.Time                             `json:"endTime"`
	CurrentTime          time.Time                             `json:"currentTime"`
	Consumers            map[string]map[string]*ConsumerStatus `json:"consumers"`
	TimeStep             string                                `json:"timeStep"`
	State                State                                 `json:"state"`
	ConsumerNames        []string                              `json:"consumerNames"`
	ConsumerLagTolerance int                                   `json:"consumerLagTolerance"`
	Count                int64                                 `json:"count"`
}

type ConsumerStatus struct {
	JetStreamConsumerInfo *jetstream.ConsumerInfo `json:"jetStreamConsumerInfo,omitempty"`
	Error                 string                  `json:"error,omitempty"`
	PendingMessages       uint64                  `json:"pendingMessages"`
	Lagging               bool                    `json:"lagging"`
}

type Start struct {
	StartTime      time.Time
	EndTime        time.Time
	NumOfInstances int
}

func (service *Service) do(ctx context.Context, method string, uri string, body any, response any) error {
	var b bytes.Buffer

	log := service.logger.With("method", method, "uri", uri, "body", body, "response", response, "requestID", uuid.New())
	log.Debug("start request")

	if body != nil {
		log.Debug("encoding body")

		if err := json.NewEncoder(io.Writer(&b)).Encode(body); err != nil {
			return fmt.Errorf("could not decode form: %w", err)
		}
	}

	log.Debug("new request with context")
	req, err := http.NewRequestWithContext(ctx, method, service.orchestrator+uri, io.Reader(&b))
	if err != nil {
		return fmt.Errorf("new request: %w", err)
	}

	log.Debug("client do")
	resp, err := service.client.Do(req)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}

	log.Debug("checking response")
	if resp.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("status code incorrect: %s", resp.Status)
	}

	if response != nil {
		log.Debug("decoding response")
		if err := json.NewDecoder(resp.Body).Decode(response); err != nil {
			return fmt.Errorf("decode failed: %w", err)
		}
	}

	return nil
}
