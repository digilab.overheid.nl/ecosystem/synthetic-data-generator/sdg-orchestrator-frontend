module gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-frontend

go 1.22.3

require (
	github.com/ajg/form v1.5.1
	github.com/alecthomas/kong v0.9.0
	github.com/alecthomas/kong-yaml v0.2.0
	github.com/go-chi/chi/v5 v5.0.12
	github.com/google/uuid v1.6.0
	github.com/nats-io/nats.go v1.35.0
)

require (
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
