FROM digilabpublic.azurecr.io/golang:1.22.3-alpine3.19

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

# Install Node.js. Note: the nodejs package does not include corepack, so we use nodejs-current
RUN apk add --no-cache nodejs-current

RUN corepack enable && corepack prepare pnpm@latest --activate

# Cache dependencies
WORKDIR /build

COPY go.mod go.sum ./

RUN go mod download

COPY package.json pnpm-lock.yaml tailwind.config.js .postcssrc .parcelrc ./

RUN pnpm install

COPY . .
 
ENTRYPOINT sh -c 'pnpm run dev & CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.html|.+\.js|.+\.css|.+\.sql)$" -exclude-dir=.git -exclude-dir=node_modules -exclude-dir=.parcel-cache -build="go build -o server ." -command="./server serve"'
