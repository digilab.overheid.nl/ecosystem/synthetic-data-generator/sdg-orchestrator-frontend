FROM golang:1.22.3-alpine3.18 AS builder

WORKDIR /build

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o server

## Stage 1-2 | Build JS
FROM node:18.20.3-alpine3.19 as js_builder

WORKDIR /build

RUN corepack enable && corepack prepare pnpm@latest --activate

COPY package.json pnpm-lock.yaml ./

RUN pnpm install

COPY . . 

RUN pnpm run build

## Run the server for dev
FROM digilabpublic.azurecr.io/alpine:3.19

EXPOSE 8080

COPY --from=builder /build/server .
COPY --from=js_builder /build/public public

CMD /server serve

